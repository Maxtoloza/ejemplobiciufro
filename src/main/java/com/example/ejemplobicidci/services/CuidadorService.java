package com.example.ejemplobicidci.services;


import com.example.ejemplobicidci.model.Cuidador;
import com.example.ejemplobicidci.repository.CuidadorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CuidadorService {

    @Autowired
    private CuidadorRepository cuidadorRepository;



    public Cuidador agregarCuidador (Cuidador nuevoCuidador){

        Cuidador cuidador = new Cuidador();


        cuidador.setIdCuidador(nuevoCuidador.getIdCuidador());
        cuidador.setNombre(nuevoCuidador.getNombre());
        cuidador.setHorarioEntrada(nuevoCuidador.getHorarioEntrada());
        cuidador.setHorarioSalida(nuevoCuidador.getHorarioSalida());
        cuidador.setLugar(nuevoCuidador.getLugar());


        System.out.println("Se creó un nuevo cuidador con exito");

     return cuidadorRepository.save(cuidador);
    }


    public List<Cuidador> obtenerCuidadores() {


        return cuidadorRepository.findAll();
    }
}
